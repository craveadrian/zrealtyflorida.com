<div id="services">
	<div class="row">
		<div class="wrapper">
			<dl class="service-box">
				<dt> <img src="public/images/content/service1.png" alt="Service image" class="firstSvc"> </dt>
				<dd>
					<p>Real Estate Classes</p>
					<a href="services#content"><img src="public/images/content/service1bot.png" alt="Bottom Service Images"></a>
				</dd>
			</dl>
			<dl class="service-box">
				<dt> <img src="public/images/content/service2.jpg" alt="Service image"> </dt>
				<dd>
					<p>Property Management <br> / Rentals</p>
					<a href="services#content"><img src="public/images/content/service2bot.png" alt="Bottom Service Images"></a>
				</dd>
			</dl>
			<dl class="service-box">
				<dt> <img src="public/images/content/service3.jpg" alt="Service image"> </dt>
				<dd>
					<p>Buyers</p>
					<a href="services#content"><img src="public/images/content/service3bot.png" alt="Bottom Service Images"></a>
				</dd>
			</dl>
			<dl class="service-box">
				<dt> <img src="public/images/content/service4.jpg" alt="Service image"> </dt>
				<dd>
					<p>Sellers</p>
					<a href="services#content"><img src="public/images/content/service4bot.png" alt="Bottom Service Images"></a>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="about">
	<div class="row">
		<div class="abtLeft fl">
			<div class="text">
				<h1>USLADYS <span>ZARZUELA</span></h1>
				<p>
					NY & FL Real Estate Broker
					<span>Zarzuela Realty</span>
					<span>O: 855.577.4549</span>
					<span>License #: 3188261</span>
				</p>
				<a href="#" class="btn">CONTACT <br> ME</a>
			</div>
		</div>
		<div class="abtRight fl">
			<img src="public/images/content/profile.jpg" alt="USLADYS ZARZUELA">
			<div class="text">
				<h3>YOUR LOCAL REAL ESTATE CONNECTION</h3>
				<p>Get a positive, helpful partner for buying or selling a home:</p>
				<ul>
					<li><span>Trusted resource for answers about the process</span></li>
					<li><span>Innovative marketing strategies</span></li>
					<li><span>Expertise about neighborhood features</span></li>
					<li><span>Ability to target home searches</span></li>
					<li><span>Strong negotiation skills</span></li>
					<li><span>Support through the closing and beyond</span></li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="service2">
	<div class="row">
		<div class="wrapper">
			<dl>
				<dt> <img src="public/images/content/service2-1.jpg" alt="SERVICES 2 image"> </dt>
				<dd>
					<h4>FIND YOUR NEXT HOME</h4>
					<p>You need someone who knows this area inside and out! I can work with you to find the right home at the right price for you...</p>
					<a href="services#content" class="more">READ MORE</a>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service2-2.jpg" alt="SERVICES 2 image"> </dt>
				<dd>
					<h4>SELL A HOME</h4>
					<p>When it’s time to move, you need someone who will advertise your home, show to prospective buyers, negotiate the purchase contract...</p>
					<a href="services#content" class="more">READ MORE</a>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service2-3.jpg" alt="SERVICES 2 image"> </dt>
				<dd>
					<h4>CONSULT ON HOME SELLING TACTICS</h4>
					<p>Oftentimes buyers don’t visualize living in your home the way you do. I can make your home attractive to its ideal audience - which can help...</p>
					<a href="services#content" class="more">READ MORE</a>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<div class="s4Left fl"></div>
		<div class="s4Right fl">
			<h2>SERVING KISSIMMEE AND<span>SURROUNDING AREAS</span></h2>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section5">
	<div class="row">
		<h2>MAKING REALTY DREAMS <span>A REALITY</span></h2>
	</div>
</div>
