<footer>
	<div id="footer">
		<div class="row">
			<div class="wrapper">
				<div class="footLeft">
					<h2>CONTACT <span>ME</span></h2>
				</div>
				<div class="footLeft2">
					<h4>CONTACT INFO</h4>
						<p><?php $this->info(["phone","tel","ft-phone"]); ?></p>
						<p><?php $this->info(["email","mailto","ft-email"]); ?></p>
						<p> <a href="https://www.google.com/maps/place/620+Dundee+Rd+Suite+A%26B,+Dundee,+FL+33838,+USA/@16.4186684,120.5912105,15z/data=!4m5!3m4!1s0x88dd0cea3c4e514d:0xcbea063ecd5893c0!8m2!3d28.0193034!4d-81.6301305" class="ft-location"><span><?php $this->info("address"); ?></span></a> </p>
						<img src="public/images/common/cards.png" alt="Cards">
				</div>
				<div class="footMid">
					<h4>NAVIGATION</h4>
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
						<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">About Us</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
						<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">Gallery</a></li>
						<li <?php $this->helpers->isActiveMenu("tesimonials"); ?>><a href="<?php echo URL ?>testimonials#content">Testimonials</a></li>
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact us</a></li>
					</ul>
				</div>
				<div class="footRight">
					<p>
						<a href="<?php $this->info("fb_link"); ?>" target="_blank">F</a>
						<a href="<?php $this->info("tt_link"); ?>" target="_blank">L</a>
						<a href="<?php $this->info("yt_link"); ?>" target="_blank">X</a>
						<a href="<?php $this->info("rs_link"); ?>" target="_blank">R</a>
					</p>
				</div>
				<div class="footRight2">
					<a href="https://www.google.com/maps/place/620+Dundee+Rd+Suite+A%26B,+Dundee,+FL+33838,+USA/@16.4186684,120.5912105,15z/data=!4m5!3m4!1s0x88dd0cea3c4e514d:0xcbea063ecd5893c0!8m2!3d28.0193034!4d-81.6301305">
						<img src="public/images/common/map.jpg" alt="">
					</a>
				</div>
			</div>
			<div class="footBot">
				<a href="<?php echo URL ?>">
					<img src="public/images/common/footLogo.png" alt="footLogo" class="footLogo">
				</a>
				<p class="copy">
					Copyright © <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved.
					<?php if( $this->siteInfo['policy_link'] ): ?>
						<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
					<?php endif ?>
				</p>
				<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
			</div>
		</div>
	</div>
</footer>
<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
				$( '.destroy-on-load' ).remove();
			})
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span class="ctc-hide">Call To Action Button</span></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
