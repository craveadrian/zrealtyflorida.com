<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="wrapper">
					<div class="hdLeft">
						<a href="<?php echo URL ?>">
							<img src="public/images/common/mainLogo.png" alt="mainLogo">
						</a>
					</div>
					<div class="hdRight">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("tesimonials"); ?>><a href="<?php echo URL ?>testimonials#content">TESTIMONIALS</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<div class="container">
					<div class="text">
						<h2>WELCOME</h2>
						<p>Become a Licensed Real Estate Sales Associate Sign up for either Weedkdays or Weekend Classes Located at 620 Dundee Road, Suite B, Dundee, FL 33838</p>
						<p>$175 <span> <a href="https://www.paypal.com/" target="_blank"> <img src="public/images/common/paypal.png" alt="paypal"> </a> </span> </p>
						<p>IF YOU SIGN UP <span>BEFORE DEC. 31,2018</span></p>
						<p>Reg. Price $350</p>
					</div>
				</div>
			</div>
		</div>
	<?php //endif; ?>
